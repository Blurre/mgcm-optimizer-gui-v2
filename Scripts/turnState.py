import math

class targetStats:
    def __init__(self, stats, resistances, special = None):
        self.stats = stats
        self.resistances = resistances
        self.special = special
    
    def getStats(self):
        return self.resistances
    
    def getResistances(self):
        return self.resistances
    
    def getSpecial(self):
        return self.special

class unitStats:
    def __init__(self, stats, skills, ldskill):
        self.stats = stats
        self.skills = skills
        self.ldskill = ldskill
    
    def getStats(self):
        return self.stats
    
    def getSkills(self):
        return self.skills
    
    def getLdSkill(self):
        return self.ldskill

# The turn state is a snapshot of the battle when it is time for the user to select the next skills
# Upon selecting the next skill it will process the skill and return the next state and who has the next skill
class turnState:
    def __init__(self, targets, units, currState, nextTurn):
        self.nextState = {}
        
        # Check Passives
        for dress in units:
            skills = dress.getSkills()
            for skill in skills:
                if skill_db[skill]["skill_type"].lower() == "passive":
                    for hit in range(skill_db[skill]["skill_hits"])
                        if skill_db[skill]["skill_buffs"][0] != '':
        
        
# Calculates the number of ticks and the gauge for the next turn
# The gauge input is the gauge that has already been modified by gauge changing mechanics
# It is essentially the "pre-gauge" columns of the mults spreadsheet
# The speeds are in order from unit 0, 1, 2, 3, target(s)
# The speeds are also already multiplied by the speed buff in turnState
def calculateGauge(speeds, gauge):
    ticks = max(min(math.ceil((100-gauge)*100/math.ceil(speeds))),1)
    gauge = ticks * speeds
    return gauge

# This function returns the speeds of the entities
# This function will also handle speed modifying skills
def getSpeeds(units, targets, buffs, debuffs):
    # Check skills for a unique skill code
    pass
    
# This function will take the buff/debuffs to be applied and return the modified turnState
def applyStatusEffect(statusEffect, targets, targetIndex, currState):

    